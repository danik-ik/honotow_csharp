﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HanoTowConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            int steps = HanoTow.HanoTowEngine.HanoTowExecute(8);
            Console.WriteLine("==== {0} steps passed", steps);
            Console.ReadKey();
        }
    }
}
