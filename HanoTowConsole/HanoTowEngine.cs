﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HanoTow
{

    class HanoTowEngine
    {
        private Stack<int>[] Towers = new Stack<int>[3];
        private int StepsPassed = 0;
        private int StoneCount;

        private HanoTowEngine() { }
        private HanoTowEngine(int StoneCount)
        {
            for (int i = 0; i <= 2; i++) { Towers[i] = new Stack<int>(); }
            for (int i = StoneCount; i >= 1; i--) { Towers[0].Push(i); }
            this.StoneCount = StoneCount;
        }

        private void RealMove(int from, int to)
        {
            int stone = Towers[from].Pop();
            Towers[to].Push(stone);
            StepsPassed++;

            // todo: use callback for visualisation
            Console.WriteLine("{0}: {1} -> {2}", stone, from, to);
        }

        private static readonly int[,] ThirdTower = {
            { 0, 2, 1 },
            { 2, 0, 0 },
            { 1, 0, 0 }
        };

        private void Move(int from, int to, int count)
        {

            switch (count)
            {
                case 0:
                    return;
                case 1: // Real move
                    RealMove(from, to);
                    break;
                default:
                    int mid = ThirdTower[from, to];
                    Move(from, mid, count - 1);
                    Move(from, to, 1);
                    Move(mid, to, count - 1);
                    break;
            }

        }

        private void Execute()
        {
            Move(0, 2, StoneCount);
        }

        public static int HanoTowExecute(int StoneCount)
        {
            HanoTowEngine it = new HanoTowEngine(StoneCount);
            it.Execute();
            return it.StepsPassed;
        }

    }

}
